<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Tabla dinamica</title>
</head>
<body>
<?php
// Define an array of products with their details
$products = array(
    array("Coca Cola", 100, number_format(4500, 0, ",", ".")),
    array("Pepsi", 30, number_format(4800, 0, ",", ".")),
    array("Sprite", 20, number_format(4500, 0, ",", ".")),
    array("Guarana", 200, number_format(4500, 0, ",", ".")),
    array("SevenUp", 24, number_format(4800, 0, ",", ".")),
    array("Mirinda Naranja", 56, number_format(4800, 0, ",", ".")),
    array("Mirinda Guarana", 89, number_format(4800, 0, ",", ".")),
    array("Fanta Naranja", 10, number_format(4500, 0, ",", ".")),
    array("Fanta Pina", 2, number_format(4500, 0, ",", "."))
);

// Define CSS styles dynamically
echo '<style>
    table {
        border-collapse: collapse;
        width: 50%;
    }

    th, td {
        border: 1px solid black;
        padding: 10px;
        text-align: center;
    }

    th {
        background-color: #bebebe;
    }

    #titulo {
        background-color: yellow;
    }

    tr:nth-child(even) {
        background-color: #bbdfbb;
    }
</style>';

// Generate the HTML table dynamically
echo '<table>
    <tr>
        <th colspan="3" id="titulo">Productos</th>
    </tr>
    <tr>
        <th>Nombre</th>
        <th>Cantidad</th>
        <th>Precio (Gs)</th>
    </tr>';
    
// Loop through the products array and generate table rows
foreach ($products as $product) {
    echo '<tr>';
    echo '<td>' . $product[0] . '</td>';
    echo '<td>' . $product[1] . '</td>';
    echo '<td>' . $product[2] . '</td>';
    echo '</tr>';
}
echo '</table>';
?>

</body>
</html>